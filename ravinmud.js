var net = require('net'), 
    fs  = require('fs'),
    _   = require("underscore"),
    Lib = {};

fs.readFile('./lib/autoloads', function(err, buf) {
  if(err) { 
    console.log("Unable to read autoloads file!");
    return;
  }
  
  var autoloads = buf.toString().split("\n");
  for(i=0;i<autoloads.length;i++) {
    var read = cleanInput(autoloads[i]).split(":");
    Lib[read[0]] = require("./lib/" + read[1] + ".js");
    console.log("Loaded " + read[0] + ":" + read[1]);
  }
  
  // Create a new server and provide a callback for when a connection occurs
  var server = net.createServer(newSocket);
  
  // Listen on port 2222
  server.listen(2222);
  console.log("Ravinmud Running on Port 2222")
});

var Players = [];

/*
 * Cleans the input of carriage return, newline
 */
function cleanInput(data) {
  return data.toString().replace(/(\r\n|\n|\r)/gm, "");
}

/*
 * Method executed when data is received from a socket
 */
function receiveData(socket, data) {
  var cleanData = cleanInput(data);
}

/*
 * Method executed when a socket ends
 */
function closeSocket(socket) {
  Players = _.without(Players, _.findWhere(Players, {socket:socket}));
}

/*
 * Callback method executed when a new TCP socket is opened.
 */
function newSocket(socket) {
  var p = new Lib.Player();
  p.connected(socket);
  Players.push(p);
  
  p.write("Ravinmud welcome message!\n");
  
  socket.on('data', function(data) {
    receiveData(socket, data);
  })
  socket.on('end', function() {
    closeSocket(socket);
  })
}

