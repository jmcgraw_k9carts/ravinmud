function Player() {
  this.socket = {};
  
  this.connected = function(socket) {
    this.socket = socket;
  }
  
  this.write = function(text) {
    this.socket.write(text);
  }
}
module.exports = Player;
